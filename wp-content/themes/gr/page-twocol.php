<?php
/*
Template Name: Två kolumner
*/
get_header(); ?>
<!-- Row for main content area -->
	<div class="small-12 large-12 columns" role="main">
	
	<?php /* Start loop */ ?>
	<?php while (have_posts()) : the_post(); ?>
		<?php get_template_part( 'content', 'twocol' ); ?>
	<?php endwhile; // End the loop ?>

	</div>
		
<?php get_footer(); ?>