<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
	<header>
		<h1 class="entry-title"><?php the_title(); ?></h1>
	</header>
	
    <?php $lang = (qtrans_getLanguage() == 'en') ? '_en' : '' ; ?>
    <?php if(get_field( 'rad' )): ?>
    	<?php $count = 1; ?>
		<?php while(has_sub_field( 'rad' )): ?>
			<?php
			$col1width = get_sub_field('layout');
			$col2width = 12 - $col1width;
			?>
			<div class="row">
				<div class="entry-content columns small-12 large-<?php echo $col1width; ?> field_<?php echo $count ?>">
					<?php
					if( $col1width == 4 ) {
						echo wp_get_attachment_image( get_sub_field('image'), 'medium' );
					} else {
						echo apply_filters( 'the_content', get_sub_field('text' . $lang) );
					}
					?>
				</div>
				<?php $count++; ?>
				<div class="entry-content columns small-12 large-<?php echo $col2width; ?> field_<?php echo $count ?>">
					<?php
					if( $col1width == 8 ) {
						echo wp_get_attachment_image( get_sub_field('image'), 'medium' );
					} else {
						echo apply_filters( 'the_content', get_sub_field('text' . $lang) );
					}
					?>
				</div>
			</div>
			<?php $count++; ?>
		<?php endwhile; ?>
	<?php endif; ?>
</article>