<?php
/*
Template Name: Multisida
*/
get_header(); ?>

<!-- Row for main content area -->
	</div>
	<div class="" role="main">
	
	<?php
	//extra menyn:
	$page_ids = get_field('pages');
	?>
	<div data-magellan-expedition="fixed" class="fixed-nav">
		<ul class="sub-nav">
			<?php foreach ($page_ids as $id) {
				$page = get_page( $id );
				?>
				<li data-magellan-arrival="post-<?php echo $page->ID; ?>">
					<a href="#post-<?php echo $page->ID; ?>"><?php echo __($page->post_title); ?></a>
				</li>
			<?php } ?>
		</ul>
	</div>


	<?php /* Start main loop */ ?>
	<?php while (have_posts()) : the_post(); ?>
		<div class="fullwidth section-1">
			<article <?php post_class('row') ?> id="post-<?php the_ID(); ?>">
				<header>
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header>
				<div class="entry-content">
					<?php the_content(); ?>
				</div>
			</article>
		</div>
	<?php endwhile; // End main loop ?>



	<?php

	$count = 2;

	foreach ($page_ids as $id) {
			/**
			 * The WordPress Query class.
			 * @link http://codex.wordpress.org/Function_Reference/WP_Query
			 *
			 */
			$args = array(
				
				//Post & Page Parameters
				'page_id' => $id,
			);
		
		$sub_page = new WP_Query( $args );

		

		while ( $sub_page -> have_posts()) : $sub_page -> the_post(); ?>
			<?php
			//ta reda på vilken template som ska användas: (lite hackigt...)
			if( get_field( 'col_1' ) && !get_field( 'col_3' ) ) {
				$template = 'twocol';
			} elseif ( get_field( 'col_3' ) && !get_field( 'col_4' ) ) {
				$template = 'threecol';
			} elseif ( get_field( 'col_4' ) ) {
				$template = 'fourcol';
			} elseif ( get_field( 'tabs' ) ) {
				$template = 'tabs';
			} elseif ( get_field( 'rad' ) ) {
				$template = 'standard';
			} else {
				$template = '';
			}
			?>
			<div class="fullwidth section-<?php echo $count; ?>">
				<div class="row" data-magellan-destination="post-<?php the_ID(); ?>">
					<?php get_template_part( 'content', $template ); ?>
					<a href="#" class="js-to-top-link" title="">
						<i class="icon-up"></i>
						<p><?php _e( 'To the top', 'reverie' ) ?></p>
					</a>
					
				</div>
			</div>
			<?php $count++; ?>
		<?php endwhile; // End the loop
		wp_reset_query();
	}

	?>


	</div>
	<div>
<?php get_footer(); ?>