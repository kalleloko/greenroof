<?php
/*
Author: Zhen Huang
URL: http://themefortress.com/

This place is much cleaner. Put your theme specific codes here,
anything else you may wan to use plugins to keep things tidy.

*/

/*
1. lib/clean.php
    - head cleanup
	- post and images related cleaning
*/
require_once('lib/clean.php'); // do all the cleaning and enqueue here
/*
2. lib/enqueue-sass.php or enqueue-css.php
    - enqueueing scripts & styles for Sass OR CSS
    - please use either Sass OR CSS, having two enabled will ruin your weekend
*/
require_once('lib/enqueue-sass.php'); // do all the cleaning and enqueue if you Sass to customize Reverie
//require_once('lib/enqueue-css.php'); // to use CSS for customization, uncomment this line and comment the above Sass line
/*
3. lib/foundation.php
	- add pagination
	- custom walker for top-bar and related
*/
require_once('lib/foundation.php'); // load Foundation specific functions like top-bar
/*
4. lib/presstrends.php
    - add PressTrends, tracks how many people are using Reverie
*/
require_once('lib/presstrends.php'); // load PressTrends to track the usage of Reverie across the web, comment this line if you don't want to be tracked

require_once('lib/fields.php');
require_once('lib/custom_post_types.php');
require_once('lib/base_functions.php');
/**********************
Add theme supports
**********************/
function reverie_theme_support() {
	// Add language supports.
	load_theme_textdomain('reverie', get_template_directory() . '/lang');
	
	// Add post thumbnail supports. http://codex.wordpress.org/Post_Thumbnails
	add_theme_support('post-thumbnails');
	// set_post_thumbnail_size(150, 150, false);
	
	// rss thingy
	add_theme_support('automatic-feed-links');
	
	// Add post formarts supports. http://codex.wordpress.org/Post_Formats
	add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));
	
	// Add menu supports. http://codex.wordpress.org/Function_Reference/register_nav_menus
	add_theme_support('menus');
	register_nav_menus(array(
		'primary' => __('Primary Navigation', 'reverie'),
		'utility' => __('Utility Navigation', 'reverie')
	));
	
	// Add custom background support
	add_theme_support( 'custom-background',
	    array(
	    'default-image' => '',  // background image default
	    'default-color' => '', // background color default (dont add the #)
	    'wp-head-callback' => '_custom_background_cb',
	    'admin-head-callback' => '',
	    'admin-preview-callback' => ''
	    )
	);
}
add_action('after_setup_theme', 'reverie_theme_support'); /* end Reverie theme support */

// create widget areas: sidebar, footer
$sidebars = array('Sidebar');
foreach ($sidebars as $sidebar) {
	register_sidebar(array('name'=> $sidebar,
		'before_widget' => '<article id="%1$s" class="row widget %2$s"><div class="small-12 columns">',
		'after_widget' => '</div></article>',
		'before_title' => '<h6><strong>',
		'after_title' => '</strong></h6>'
	));
}
$sidebars = array('Footer');
foreach ($sidebars as $sidebar) {
	register_sidebar(array('name'=> $sidebar,
		'before_widget' => '<article id="%1$s" class="large-4 columns widget %2$s">',
		'after_widget' => '</article>',
		'before_title' => '<h6><strong>',
		'after_title' => '</strong></h6>'
	));
}

// return entry meta information for posts, used by multiple loops.
function reverie_entry_meta() {
	echo '<time class="updated" datetime="'. get_the_time('c') .'" pubdate>'. sprintf(__('Posted on %s at %s.', 'reverie'), get_the_time('l, F jS, Y'), get_the_time()) .'</time>';
	echo '<p class="byline author">'. __('Written by', 'reverie') .' <a href="'. get_author_posts_url(get_the_author_meta('ID')) .'" rel="author" class="fn">'. get_the_author() .'</a></p>';
}

//ta bort admin bar
function hide_admin_bar() {
    return false;
}
add_filter( 'show_admin_bar', 'hide_admin_bar' );

// REMOVE THE WORDPRESS UPDATE NOTIFICATION FOR ALL USERS EXCEPT SYSADMIN
global $user_login;
get_currentuserinfo();
if (!current_user_can('update_plugins')) { // checks to see if current user can update plugins
  add_action( 'init', create_function( '$a', "remove_action( 'init', 'wp_version_check' );" ), 2 );
  add_filter( 'pre_option_update_core', create_function( '$a', "return null;" ) );
}

// admin style
function gr_custom_admin_style() {
  echo '<style type="text/css">
    #acf-startdate, #acf-starttime, #acf-enddate, #acf-endtime {
      display: inline-block;
      width: 45%;
    }
    div[id^="acf-visit_moment"],
    div[id^="acf-tab_heading"] {
      width: 24%;
      display: inline-block;
    }
    div[id^="acf-visit_text"],
    div[id^="acf-tab_text"] {
      width: 70%;
      display: inline-block;
    }
    div[id^="acf-visit_moment"].acf-tab_group-hide,
    div[id^="acf-tab_heading"].acf-tab_group-hide {
      display: none;
    }
    div[id^="acf-visit_text"].acf-tab_group-hide,
    div[id^="acf-tab_text"].acf-tab_group-hide {
      display: none;
    }
    div[id^="acf-tab_heading"],
    div[id^="acf-tab_text"] {
      height: 400px;
    }
  </style>';
}

add_action('admin_head', 'gr_custom_admin_style');


//eventdatum
function gr_eventdate( $echo = true ) {
  $return = '<div class="date-wrap">';
  $return .= '<div class="date">';
  if( !get_field( 'startdate' ) ) return;
  
  $startdate = get_field( 'startdate' );
  $enddate = get_field( 'enddate' );
  
  $return .= '<span class="month">';
  if( date('Y', strtotime($startdate)) != date('Y') ) {
    $return .= '<span class="year">'  . date('Y', strtotime($startdate)) . '</span>';
  }
  $return .= date('M', strtotime($startdate)) . '</span>';
  $return .= '<span class="day">'  . date('j', strtotime($startdate)) . '</span>';
  $return .= '</div>';
  
  if( !empty($enddate) && $startdate != $enddate ) {
    $return .= '<span class="date-sep">' . __( '↓', 'reverie' ) . '</span>';
    $return .= '<div class="date">';
    $return .= '<span class="month">';
    if( date('Y', strtotime($enddate)) != date('Y') ) {
      $return .= '<span class="year">'  . date('Y', strtotime($enddate)) . '</span>';
    }
    $return .=  date('M', strtotime($enddate)) . '</span>';
    $return .= '<span class="day">'  . date('j', strtotime($enddate)) . '</span>';
    
    $return .= '</div>';
  }
  $return .= '</div>';
  
  if(!$echo) return $return;
  echo $return;
}

//eventtid
function gr_eventtime( $echo = true ) {
  if( !get_field( 'starttime' ) ) return;
  
  $return = '<p class="time-wrap">';
  $return .= '<span class="meta-label">' . __( 'Time:', 'reverie' ) . ' </span>';
  
  $starttime = get_field( 'starttime' );
  $endtime = get_field( 'endtime' );
  
  $startdate = get_field( 'startdate' );
  $enddate = get_field( 'enddate' );
  
  $one_day_event = false;
  if( empty($enddate) || $startdate == $enddate ) {
    $one_day_event = true;
  }
  
  //skriv ut datum ifall flerdagars-event
  if( !$one_day_event ) {
    $return .= '<span class="inline-date">';
    $return .= '<span class="day">' . date('j', strtotime($startdate)) . '</span>';
    $return .= '<span class="date-slash">/</span>';
    $return .= '<span class="month">' . date('m', strtotime($startdate)) . '</span>';
    $return .= '<span class="date-comma">, </span>';
    $return .= '</span>';
  }
  
  //starttiden
  $return .= '<span class="hour">' . substr( $starttime, 0, 2) . '</span>';
  $return .= '<span class="time-colon">:</span>';
  $return .= '<span class="min">' . substr( $starttime, 3, 2) . '</span>';
  
  if( !empty($endtime) ) {
    $return .= '<span class="time-sep"> – </span>';
    
    //skriv ut datum ifall flerdagars-event
    if( !$one_day_event ) {
      $return .= '<span class="inline-date">';
      $return .= '<span class="day">'  . date('j', strtotime($enddate)) . '</span>';
      $return .= '<span class="date-slash">/</span>';
      $return .= '<span class="month">' . date('m', strtotime($enddate)) . '</span>';
      $return .= '<span class="date-comma">, </span>';
      $return .= '</span>';
    }
    
    //sluttiden
    $return .= '<span class="hour">' . substr( $endtime, 0, 2) . '</span>';
    $return .= '<span class="time-colon">:</span>';
    $return .= '<span class="min">' . substr( $endtime, 3, 2) . '</span>';
  }
  $return .= '</p>';
  
  if(!$echo) return $return;
  echo $return;
}



/**
* Language list Code for non-Widget users
*
* @global array $q_config
* @param string $sep
*/
function qtrans_generate_language_list($sep = " | ") {
  global $q_config;
  $languages = qtrans_getSortedLanguages();
  $num_langs = count($languages);
  $id = 'qtranslate-chooser';
  $url = is_404() ? get_option('home') : '';
  
  echo '<li class="divider"></li>';
  
  foreach ($languages as $language) {
    $classes = array('lang-' . $language);
    if ($language == $q_config['language'])
      $classes[] = 'active';
     
    echo '<li class="' . implode(' ', $classes) . '"><a href="' . trailingslashit(qtrans_convertURL($url, $language)) . '"';
    echo ' hreflang="' . $language . '" title="' . $q_config['language_name'][$language] . '"';
    echo '>' . $language . '</a></span>';
     
    if (--$num_langs > 0) {
      echo '<li class="divider"></li>';
    }
  }
}

/**
 * get_field_translated()
 *
 * funkar som get_field
 * bygger på att det finns sv och eng fält, xxx och xxx_en eller yyy_1 och yyy_en_1
 *
 */

function get_field_translated( $field ) {
  $sv = get_field( $field );
  $en = get_field( $field . '_en' );
  if ( qtrans_getLanguage() == 'sv' ) {
    if ( empty($sv) ) return false;
  }
  if ( qtrans_getLanguage() == 'en' ) {
    if ( empty($en) ) return false;
  }
  return __( '<!--:sv-->' . $sv . '<!--:--><!--:en-->' . $en . '<!--:-->' );
}

/**
 * the_field_translated()
 *
 * funkar som the_field
 * bygger på att det finns sv och eng fält, xxx och xxx_en eller yyy_1 och yyy_en_1
 *
 */

function the_field_translated( $field ) {
  echo get_field_translated( $field );
}

add_action('acf/register_fields', 'my_register_fields');
function my_register_fields() {
  include_once('lib/acf-repeater/repeater.php');
}

function get_page_by_slug($page_slug, $output = OBJECT, $post_type = 'page' ) {
    global $wpdb;
    $page = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type= %s", $page_slug, $post_type ) );
    if ( $page )
            return get_page($page, $output);
    return null;
}

if( strpos($_SERVER['REQUEST_URI'], 'int-technical-visits') ){
  wp_redirect( home_url('/technical-visits') );
  exit;
}

?>
