<?php
/*
Template Name: Hem
*/
get_header(); ?>

<!-- Row for main content area -->
  </div>
  <div class="orbit-wrapper hide-for-small">
    <?php //orbit slider: ?>
    <?php $lang = (qtrans_getLanguage() == 'en') ? '_en' : '' ; ?>
    <?php if(get_field( 'bild' )): ?>
        <ul data-orbit data-options="bullets:false; pause_on_hover: false; slide_number: false;">
          <?php while(has_sub_field( 'bild' )): ?>
          <li>
            <div class="hero-text"><?php the_sub_field( 'text' . $lang ); ?>
              <?php if( get_sub_field( 'link_to' ) ) { ?>
                <a class="button" href="<?php the_sub_field( 'link_to' ); ?>"><?php _e( 'Read more', 'reverie' ) ?></a>
              <?php } ?>
            </div>
            <?php echo wp_get_attachment_image( get_sub_field( 'image' ), 'full' ); ?>
          </li>
        <?php endwhile; ?>
        </ul>
    <?php endif; ?>
  </div>

  <section class="call-to-actions">
    <div class="row">
      <?php
      for( $i=1; $i<=5; $i++ ){
        if( get_field('push_text_' . $i) ) { ?>
          <article class="small-6 large-3 columns">
            <?php if( get_field( 'push_link_' . $i ) ) { ?>
              <a href="<?php the_field( 'push_link_' . $i ); ?>">
            <?php } ?>
            <figure class="img-wrap">
              <figure class="image">
                <?php echo wp_get_attachment_image( get_field( 'push_img_' . $i ), 'small' ); ?>
              <?php if( get_field( 'push_link_' . $i ) ) { ?>
                </a>
              <?php } ?>
              </figure>
            </figure>
            <section class="push-text">
              <?php the_field_translated( 'push_text_' . $i ); ?>
              <?php if( get_field( 'push_link_' . $i ) ) { ?>
                <a href="<?php the_field( 'push_link_' . $i ); ?>"><?php _e('Read more', 'reverie') ?></a>
              <?php } ?>
            </section>
          </article>
        <?php
        }
      }
      ?>
    </div>
  </section>

  <section class="members-section">
    <div class="row">

      <?php if(get_field( 'platina_members' )): ?>
        <article class="columns members">
          <header>
            <h2><?php _e( 'Platina Members', 'reverie' ) ?></h2>
          </header>
          <?php while(has_sub_field( 'platina_members' )): ?>
            <section class="large-6 small-6 columns">
              <?php
                $img_attr = wp_get_attachment_image_src( get_sub_field( 'image' ), 'full' );
                $img_src = $img_attr[0];
                $img_id = preg_replace('/[^\w\d]/', '-', preg_replace( '/.\w{3}$/', '', basename($img_src))) . '_logo';
              ?>
              <a id="<?php echo $img_id; ?>" class="members-link" href="<?php the_sub_field( 'link' ); ?>" style="background-image: url(<?php echo $img_src; ?>)" title="<?php the_sub_field( 'hover_text' ) ?>">
              </a>
            </section>
          <?php endwhile; ?>
        </article>
      <?php endif; ?>

      <?php if(get_field( 'gold_members' )): ?>
        <article class="columns members">
          <header>
            <h3><?php _e( 'Gold Members', 'reverie' ) ?></h3>
            <?php $members_id = get_page_by_slug( 'members' ); ?>
          </header>
          <?php while(has_sub_field( 'gold_members' )): ?>
            <section class="large-3 small-6 columns">
              <?php
                $img_attr = wp_get_attachment_image_src( get_sub_field( 'image' ), 'full' );
                $img_src = $img_attr[0];
                $img_id = preg_replace('/[^\w\d]/', '-', preg_replace( '/.\w{3}$/', '', basename($img_src))) . '_logo';
              ?>
              <a id="<?php echo $img_id; ?>" class="members-link" href="<?php the_sub_field( 'link' ); ?>" style="background-image: url(<?php echo $img_src; ?>)" title="<?php the_sub_field( 'hover_text' ) ?>">
              </a>
            </section>
          <?php endwhile; ?>
          <p><?php printf(__( 'See all our members %1s here %2s', 'reverie' ), '<a href="' . get_permalink( $members_id ) . '">', '</a>') ?></p>
        </article>
      <?php endif; ?>

    </div><!-- Row End -->
  </section><!-- Members section End -->
<?php get_footer(); ?>