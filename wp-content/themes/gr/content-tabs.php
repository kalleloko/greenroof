<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
	<header>
		<h1 class="entry-title"><?php the_title(); ?></h1>
	</header>

	<?php
		$content = get_field_translated( 'field_tab_full' );
		
		if( !empty( $content ) ) { ?>
		<div class="entry-content row">
			<section class="large-12 columns">
				<?php echo $content; ?>
			</section>
		</div>
		<?php } ?>

		<!-- tabs -->
		<?php 
		$lang = (qtrans_getLanguage() == 'en') ? '_en' : '' ;
	?>
	<?php if(get_field( 'tabs' . $lang )): ?>
		<div class="section-container vertical-tabs" data-section="vertical-tabs">
			<?php $i=1; ?>
			<?php while(has_sub_field( 'tabs' . $lang )): ?>
				<section<?php if($i==1) echo ' class="active"' ?>>
					<p class="title" data-section-title>
						<a href="#"><?php the_sub_field('heading' . $lang); ?></a>
					</p>
					<div class="content" data-section-content>
						<?php the_sub_field('content' . $lang); ?>
					</div>
				</section>
				<?php $i++; ?>
			<?php endwhile; ?>
		</div>
	<?php endif; ?>

</article>