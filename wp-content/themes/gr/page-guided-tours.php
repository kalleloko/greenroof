<?php
/*
Template Name: Guidade turer-sidmall
*/
get_header(); ?>
<!-- Row for main content area -->
	<div class="small-12 large-12 columns" role="main">
	
	<?php /* Start loop */ ?>
	<?php while (have_posts()) : the_post(); ?>
		<?php get_template_part( 'content', 'twocol' ); ?>
        <?php while(has_sub_field( 'images' )): ?>
            <div class="columns large-3 small-6">
                <?php echo wp_get_attachment_image( get_sub_field('image'), 'medium' ); ?>
            </div>
        <?php endwhile; ?>
        
		<a href="#" data-reveal-id="form-modal-<?php the_ID(); ?>" class="button round expand"><?php _e( 'Send request to book the tour', 'reverie' ); ?></a>
	<?php endwhile; // End the loop ?>

	</div>
	
	<?php
        $modals .= '<div id="form-modal-' . get_the_ID() . '" class="reveal-modal form-modal guided-tour-form">';
        $modals .= '<div class="row"><h2>' . sprintf(__( 'Book the guided tour', 'reverie' ), ' <em>' . get_the_title() . '</em>') . '</h2></div>';
        $modals .= '<div class="row">';
        if( qtrans_getLanguage() == 'en' ) {
          $modals .= do_shortcode('[contact-form-7 id="1000" title="Technical visit form"]');
        } else {
          $modals .= do_shortcode('[contact-form-7 id="952" title="Teknisk Besök-formulär"]');
        }
        
        $modals .= '<a class="close-reveal-modal">&#215;</a>';
        $modals .= '</div>';
        $modals .= '</div>';
    ?>

    <?php $GLOBALS['modals'] = $modals; ?>

<?php get_footer(); ?>