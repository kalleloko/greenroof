<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @subpackage Reverie
 * @since Reverie 4.0
 */
?>

<article <?php post_class( 'row' ) ?> id="post-<?php the_ID(); ?>">
	<header class="small-9 small-offset-3 large-10 large-offset-2 columns">
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php _e( 'Read more about this event', 'reverie' ); ?>"><?php the_title(); ?></a></h1>

	</header>
	<div class="small-3 large-2 columns">
		<?php gr_eventdate(); ?>
	</div>
	
	<section class="entry-content small-9 large-8 columns">
		<div class="event-meta">
			<?php gr_eventtime(); ?>
			<?php if( get_field_translated( 'arranger' ) ) { ?>
				<p class="event-arranger"><span class="meta-label"><?php _e( 'Arranged by', 'reverie' ); ?>:</span><?php the_field_translated( 'arranger' ); ?></p>
		  	<?php } ?>
		  	<?php if( get_field( 'location' ) ) { ?>
				<p class="event-location"><span class="meta-label"><?php _e( 'Location', 'reverie' ); ?>:</span><?php the_field( 'location' ); ?></p>
		  	<?php } ?>
		  	<?php if( get_field( 'price' ) ) { ?>
				<p class="event-price"><span class="meta-label"><?php _e( 'Price', 'reverie' ); ?>:</span><?php the_field( 'price' ); ?></p>
		  	<?php } ?>
		  	<?php if( get_field( 'url' ) ) { ?>
				<p class="event-url"><span class="meta-label"><?php _e( 'Link', 'reverie' ); ?>:</span><a href="<?php the_field('url') ?>"><?php the_field( 'url' ); ?></a></p>
		  	<?php } ?>
		</div>
		<?php if( is_archive() ) : ?>
			<?php the_excerpt(); ?>
			<?php $lang = qtrans_getLanguage(); ?>
			<?php echo '<a class="readmore-link" href="' . qtrans_convertURL(get_permalink(), $lang) . '">' . __( 'Read more', 'reverie' ) . '</a>'; ?>
		<?php else: ?>
			<?php the_content(); ?>
			
		<?php endif; ?>
	</section>
	
	<div class="small-9 small-offset-3 large-2 large-offset-0 columns">
		<?php the_post_thumbnail( ); ?>
	</div>
	
</article>