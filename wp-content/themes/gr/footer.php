			</div><!-- Row End -->
		</section><!-- Container End -->
	</div>
</div> <!-- Wrap End -->
<footer>
	<div class="row footer-content" role="contentinfo">
		<div class="small-12 large-4 columns">
			<p><a href="<?php echo home_url(); ?>"><?php bloginfo( 'name' ); ?></a></p>
		</div>

		<div class="small-12 large-4 columns">
			<p>© 2014 Greenroof.</p>
			<!-- <p class="vcard">
		      <?php _e( 'Titel:', 'reverie' ) ?>
		      <br/>
		      <em class="fn">Jonatan Malmberg</em>
		      <br/>
		      <span class="tel">+46 (0)40-94 85 20</span>
		      <a class="email" href="mailto:address@greenroof.se" title="Maila Joakim">address@greenroof.se</a>
		    </p>
		    <?php $contact_id = get_page_by_slug( 'contact' ); ?>
		    <p><a href="<?php the_permalink( $contact_id ); ?>"><?php _e( 'Kontakt', 'reverie' ) ?></a></p> -->
		</div>


		<div class="small-12 large-4 columns">
			<p>
		      <a class="fb-link" href="https://www.facebook.com/pages/Augustenborg-Botanical-Roof-Garden/168025609969289">
		      	<?php _e( 'Follow us on Facebook!', 'reverie' ) ?>
		      </a>
		      	<a href="https://www.facebook.com/pages/Augustenborg-Botanical-Roof-Garden/168025609969289" target="_blank" id="icon-facebook">Facebook</a>
		    </p>
		</div>
	</div>
	
</footer>

<?php wp_footer(); ?>

<script>
	(function($) {
		// $(document).foundation('orbit', {
		// 	animation: 'slide',
		// 	slide_number: false,
		// 	animation_speed: 1000,
		// 	bullets: false
		// });
		$(document).foundation();
	})(jQuery);
</script>
<?php echo $GLOBALS['modals']; ?>
</body>
</html>