<?php get_header(); ?>

<!-- Row for main content area -->
	
<?php /* Start loop */ ?>
<?php while (have_posts()) : the_post(); ?>
	<div class="small-12 large-8 columns" role="main">
	
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
  			<p class="breadcrumb"><a href="<?php echo get_post_type_archive_link( 'visit' ); ?>"><?php _e( 'Technical Visit', 'reverie' ); ?></a>:</p>
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</header>
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
		</article>

	</div>

	<!-- sidebar -->
	<div class="small-12 large-4 columns">
		<?php 
			$lang = (qtrans_getLanguage() == 'en') ? '_en' : '' ;
		?>
		<?php if(get_field( 'moment' . $lang )): ?>
			<h3><?php _e( 'Content', 'reverie' ); ?></h3>
			<table>
				<tbody>
					<?php $i=1; ?>
					<?php while(has_sub_field( 'moment' . $lang )): ?>
						<tr>
							<th><?php echo $i . '. '; the_sub_field('moment_type' . $lang); ?>:</th>
							<td><p><?php the_sub_field('description' . $lang); ?></p></td>
						</tr>
						<?php $i++; ?>
					<?php endwhile; ?>
				</tbody>
			</table>
		<?php endif; ?>

		<a href="#" data-reveal-id="form-modal" class="button round expand"><?php _e( 'Sign up', 'reverie' ); ?></a>

		<h3><?php _e( '...or explore the other visits', 'reverie' ) ?></h3>

		<?php $main_title = get_the_title(); ?>
		
		<ul id="other_visits">
			<?php
			/**
			 * The WordPress Query class.
			 * @link http://codex.wordpress.org/Function_Reference/WP_Query
			 *
			 */
			$args = array(
				
				//Type & Status Parameters
				'post_type'   => 'visit',
				
				//Pagination Parameters
				'posts_per_page'         => -1,
			);
			
			$visits = new WP_Query( $args );

			if ( $visits -> have_posts() ) : while ( $visits -> have_posts() ) : $visits -> the_post(); ?>
				<?php
					if ( $main_title == get_the_title() ) {
						continue;
					}
				?>
				<li>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
						<?php the_title(); ?>
					</a>
				</li>
			<?php endwhile; ?>
			<!-- post navigation -->
			<?php endif; ?>
			<?php wp_reset_query(); ?>
		</ul>
	</div>
	
	<div id="form-modal" class="reveal-modal">
	  	<h2><?php _e( 'Sign up for', 'reverie' ); ?> <em><?php the_title(); ?></em></h2>
	  	<p><a href="<?php echo get_post_type_archive_link( 'visit' ); ?>"><?php _e( 'Sign up for other visits', 'reverie' ); ?></a></p>
	  	<div class="row">
	    	<?php
	    		if( qtrans_getLanguage() == 'en' ) {
					echo do_shortcode('[contact-form-7 id="1000" title="Technical visit form"]');
	    		} else {
	    			echo do_shortcode('[contact-form-7 id="952" title="Teknisk Besök-formulär"]');
	    		}
	    	?>
	    	 <a class="close-reveal-modal">&#215;</a>
	  	</div>
	</div>
<?php endwhile; // End the loop ?>
		
<?php get_footer(); ?>