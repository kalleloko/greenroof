<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
	<header>
		<h1 class="entry-title"><?php the_title(); ?></h1>
	</header>

	<?php
		$content = get_field_translated( 'field_twocol_full' );
		//echo implode("-", str_split($content, 1));
		if( !empty( $content ) ) { ?>
		<div class="entry-content row full-width">
				<section class="large-12 columns full-column">
				<?php echo $content; ?>
				</section>
		</div>
		<?php }
	?>

	<div class="entry-content row two-columns">
		<?php if ( get_field( 'col_1_width' ) ) {
			$col_1 = get_field( 'col_1_width' );
			$col_2 = 12 - $col_1;
		} else {
			$col_1 = 6;
			$col_2 = 6;
		}?>
		<section class="large-<?php echo $col_1; ?> columns left-column">
			<?php the_field_translated( 'col_1' ); ?>
		</section>
		<section class="large-<?php echo $col_2; ?> columns right-column">
			<?php the_field_translated( 'col_2' ); ?>
		</section>
	</div>

</article>