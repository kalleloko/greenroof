<?php get_header(); ?>

<!-- Row for main content area -->
	<div class="small-12 large-12 columns" role="main">
  	<?php $lang = (qtrans_getLanguage() == 'en') ? '_en' : '' ; ?>
    <?php
      $args = array(
        'post_type' => 'page',
        'pagename' => 'int-technical-visits',
      );
      
      $tours = new WP_Query( $args );
    ?>
    <?php if ( $tours -> have_posts() ) : while ( $tours -> have_posts() ) : $tours -> the_post(); ?>
    
      <article class="technical-visits-main">
        <header>
          <h1 class="entry-title"><?php the_title(); ?></h1>
        </header>
        
        <section>
          <?php the_content(); ?>
        </section>
        
      </article>
    <?php endwhile; ?>
    <?php endif; ?>
    <?php wp_reset_query(); ?>
    
    <?php $count = 0; ?>

    <?php $modals = ''; ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <?php if ( $count % 2 == 1 )
        //echo '<div class="row">';
      ?>
      <article class="technical-visit row">
        <header class="columns small-12 large-10 large-offset-2">
          <h3 class="entry-title"><?php the_title(); ?></h3>
        </header>

        <section class="excerpt columns small-12 large-10 push-2">
          <?php the_excerpt(); ?>
          <p>
            <a href="<?php the_permalink(); ?>" title="<?php _e( 'More about this visit', 'reverie' ); ?>" class="js-read-more-link" data-target=".full-<?php the_ID(); ?>" id="more-link-<?php the_ID(); ?>" data-less-link="#less-link-<?php the_ID(); ?>" ><?php _e( 'Read more', 'reverie' ); ?></a>
            <a href="<?php the_permalink(); ?>" title="<?php _e( 'More about this visit', 'reverie' ); ?>" class="js-read-less-link" data-target=".full-<?php the_ID(); ?>" id="less-link-<?php the_ID(); ?>" data-less-link="#more-link-<?php the_ID(); ?>"><?php _e( 'Hide description', 'reverie' ); ?></a>
          </p>
        </section>
        <section class="book columns small-12 large-2 pull-10">
          <a href="#" data-reveal-id="form-modal-<?php the_ID(); ?>" class="button round expand"><?php _e( 'Send Request', 'reverie' ); ?></a>
        </section>
        
        <div class="clearfix"></div>

        <section class="full-content full-<?php the_ID(); ?>">
          <div class="columns push-2 small-12 large-10">
            <?php the_content(); ?>
          </div>
          <?php if(get_field( 'moment' . $lang )): ?>
            <div class="columns small-12 large-10">
              <h3><?php _e( 'Content', 'reverie' ); ?></h3>
              <table>
                <tbody>
                  <?php $i=1; ?>
                  <?php while(has_sub_field( 'moment' . $lang )): ?>
                    <tr>
                      <th><?php echo $i . '. '; the_sub_field('moment_type' . $lang); ?>:</th>
                      <td><p><?php the_sub_field('description' . $lang); ?></p></td>
                    </tr>
                    <?php $i++; ?>
                  <?php endwhile; ?>
                </tbody>
              </table>
            </div>
          <?php endif; ?>
        </section>

        <hr>
        

        <?php
        $modals .= '<div id="form-modal-' . get_the_ID() . '" class="reveal-modal form-modal tech-visit-form">';
        $modals .= '<div class="row"><h2>' . sprintf(__( 'Book the %1s visit', 'reverie' ), ' <em>' . get_the_title() . '</em>') . '</h2></div>';
        $modals .= '<div class="row">';
        if( qtrans_getLanguage() == 'en' ) {
          $modals .= do_shortcode('[contact-form-7 id="1000" title="Technical visit form"]');
        } else {
          $modals .= do_shortcode('[contact-form-7 id="952" title="Teknisk Besök-formulär"]');
        }
        
        $modals .= '<a class="close-reveal-modal">&#215;</a>';
        $modals .= '</div>';
        $modals .= '</div>';
        ?>
        
      </article>
      <?php if ( $count % 2 == 1 )
        //echo '</div>';

      $count++;
      ?>
    <?php endwhile; ?>
    <?php endif; ?>
    
    <?php $GLOBALS['modals'] = $modals; ?>

  </div>
  		
<?php get_footer(); ?>