<?php get_header(); ?>

<!-- Row for main content area -->
	<div class="events small-12 columns" role="main">
	
	<?php /* Start loop */ ?>
	<?php while (have_posts()) : the_post(); ?>
		<?php $lang = qtrans_getLanguage(); ?>
		<?php echo '<a class="backtoarchive-link" href="' . qtrans_convertURL(home_url('/events'), $lang) . '">« ' . __( 'Back to calendar', 'reverie' ) . '</a>'; ?>
		<?php get_template_part( 'content', get_post_type() ); ?>
		
	<?php endwhile; // End the loop ?>

	</div>
		
<?php get_footer(); ?>