<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
	<header>
		<h1 class="entry-title"><?php the_title(); ?></h1>
	</header>

	<div class="orbit-wrapper">
    <?php //orbit slider: ?>
    <?php $lang = (qtrans_getLanguage() == 'en') ? '_en' : '' ; ?>
    <?php if(get_field( 'orbit_bild' )): ?>
        <ul data-orbit data-options="bullets:false; pause_on_hover: false; slide_number: false;">
          <?php while(has_sub_field( 'orbit_bild' )): ?>
          <li>
            <a href="<?php the_sub_field( 'link_to' ); ?>">
              <?php echo wp_get_attachment_image( get_sub_field( 'image' ), 'full' ); ?>
            </a>
            <div class="orbit-caption ">
              <div class="orbit-caption-wrap">
                <?php the_sub_field( 'text' . $lang ); ?>
              </div>
            </div>
          </li>
        <?php endwhile; ?>
        </ul>
    <?php endif; ?>
  </div>

</article>