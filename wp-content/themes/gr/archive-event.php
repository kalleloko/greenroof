<?php get_header(); ?>

<!-- Row for main content area -->
	<div class="events large-12 columns" role="main">
	
 	<?php
 	$view_old = false;
 	$order = 'ASC';
 	if ( isset( $_GET['view'] ) && $_GET['view'] == 'old') {
 		$view_old = true;
 		$order = 'DESC';
 	}
	//--------------------------------------- kalender
	$args = array(
    	'post_type' => 'event',
    	'posts_per_page' => -1,
    	'orderby' => 'meta_value',
    	'meta_key' => 'startdate',
    	'order' => $order,
    );
	$events = new WP_query($args);

	//print_obj($events);

	if ($events->have_posts()) : 
		if ( $view_old ) { ?>
			<div id="event-link">
				<a href="<?php echo remove_query_arg( 'view' ); ?>"><?php _e( 'See future events...', 'reverie' ) ?></a>
			</div>
			<h2><?php _e( 'Past events', 'reverie' ) ?></h2>
		<?php } 
		
		while ($events->have_posts()) : $events->the_post();
		$display = true;
		
		$enddate = get_field('enddate');
		$enddate = empty( $enddate ) ? get_field('startdate') : get_field('enddate');
		$enddate = str_replace( '-', '', $enddate );
		
		if ( $view_old ) {
			if( $enddate >= date('Ymd'))
				$display = false;
		} else {
			if( $enddate < date('Ymd'))
				$display = false;
		}

		$count = 0;
		if( $display ) {
			get_template_part( 'content', get_post_type() );
			$count++;
		}
		
	endwhile; 

	if( $count === 0 ) {
		get_template_part( 'content', 'none' );
	}
	?>
    
    <?php else : ?>
  		<?php get_template_part( 'content', 'none' ); ?>
  			
    <?php endif; // end have_posts() check ?>
    
    <?php wp_reset_query(); ?>
	
	<div id="event-link">
		<?php if ( $view_old ) { ?>
			<a href="<?php echo remove_query_arg( 'view' ); ?>"><?php _e( 'See future events...', 'reverie' ) ?></a>
		<?php } else { ?>
			<a href="<?php echo add_query_arg( 'view', 'old' ); ?>"><?php _e( 'See past events...', 'reverie' ) ?></a>
		<?php } ?>
	</div>
	
	<?php /* Display navigation to next/previous pages when applicable */ ?>
	<?php if ( function_exists('reverie_pagination') ) { reverie_pagination(); } else if ( is_paged() ) { ?>
		<nav id="post-nav">
			<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'reverie' ) ); ?></div>
			<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'reverie' ) ); ?></div>
		</nav>
	<?php } ?>

	</div>
	<div class="clearfix"></div>
		
<?php get_footer(); ?>