<?php get_header(); ?>

<!-- Row for main content area -->
	<div class="columns" role="main">
	
	<?php /* Start loop */ ?>
	<?php while (have_posts()) : the_post(); ?>
		<?php get_template_part( 'content', 'standard' ); ?>
	<?php endwhile; // End the loop ?>

	</div>
		
<?php get_footer(); ?>