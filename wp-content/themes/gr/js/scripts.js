jQuery(document).ready(function($){

	function priceFormat(price) {
		price = price.toString();
		price = price.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ");
		return price;
	}
	function numberFormat(number) {
		return parseInt( number.replace(' ', '') );
	}
	function dayOfWeek(d) {
		var dateParts = d.split("-")
        var currentDate = Math.round(+new Date( dateParts[0], dateParts[1] - 1, dateParts[2] ));
        var nData = new Date(currentDate);
        var day = nData.getDay()
        return nData.getDay();
    }

	//smooth scrolling på multi-sida:
	if ( $('.sub-nav a').length > 0 ) {
		$('.sub-nav a').bind('click',function(event){
			var $anchor = $(this);
			$('html, body').stop().animate({
				scrollTop: $($anchor.attr('href')).offset().top
			}, 500);
			event.preventDefault();
		});
	}
	if ( $('.js-to-top-link').length > 0 ) {
		$('.js-to-top-link').bind('click',function(event){
			$('html, body').stop().animate({
				scrollTop: 0
			}, 500);
			event.preventDefault();
		});
	}
	//öppna/stäng beskrivning tech visits:
	if ( $('.js-read-more-link').length > 0 ) {
		$('.js-read-more-link').click( function(e){
			var target = $(this).attr('data-target');
			var less_link = $(this).attr('data-less-link');
			$(target).slideDown();
			$(this).hide();
			$(less_link).show();
			e.preventDefault();
		});
		$('.js-read-less-link').click( function(e){
			var target = $(this).attr('data-target');
			var less_link = $(this).attr('data-less-link');
			$(target).slideUp();
			$(this).hide();
			$(less_link).show();
			e.preventDefault();
		});
	}

	//Lägger till klass till element
	$('.section-2 .section-container a').click(function(event){
	    $('.section-2 section').find('.content .fade-effect').removeClass('fade-effect');
	    $(this).parents('.section-2 section').find('.content p').addClass('fade-effect');
	})
	//formulären
	if ( $('.form-modal').length > 0 ) {
		var form = 'guidedtour';
		if($('.form-modal').hasClass('tech-visit-form')) {
			form = 'techvisit';
		}
		$('.visit-date input').datepicker({
	        dateFormat	: 'yy-mm-dd',
	        firstDay	: 1
	    });
		//uppdatera all parallellt;
		$('input[type="text"]').bind('keyup', function() {
			var name = $(this).attr('name');
			var val = $(this).val();
			$('input[name="' + name + '"]').val(val);
		});
		$('textarea').bind('keyup', function() {
			var name = $(this).attr('name');
			var val = $(this).val();
			$('textarea[name="' + name + '"]').val(val);
		});
		//betalningsmetod:
		function togglePayMeth(input) {
			var val = input.val();
			if (val == 'at-location') {
				$('p.at-location').show();
				$('p.invoice').hide();
				$('div.address').fadeOut();
			} else {
				$('p.at-location').hide();
				$('p.invoice').show();
				$('div.address').fadeIn();
			};
		}
		$('.pay_method input').click(function() {
			togglePayMeth($(this));
			$('.pay_method input[value="' + $(this).attr('value') + '"]').attr('checked', 'checked');
		});
		//sätt betalningsmetod initialt:
		$('.pay_method input[checked="checked"]').each(function() {
			togglePayMeth($(this));
		});
		//studentrabatt
		$('span.student-discount').hide();
		$('input[value="student-discount"]').click(function() {
			var val = $(this).attr('checked')? $(this).attr('checked') : false;
			if (val) {
				$('span.student-discount').show();
			} else {
				$('span.student-discount').hide();
			}
			//checka övriga
			$('input[value="student-discount"]').attr('checked', val);
			updatePrice();
		});
		//fikakostnad
		$('input[value="fikabrod"]').click(function() {
			var val = $(this).attr('checked')? $(this).attr('checked') : false;
			//checka övriga
			$('input[value="fikabrod"]').attr('checked', val);
			updatePrice();
		});
		//besökstid
		$('.visit-time input').bind('keyup', function() {
			updatePrice();
		});
		//antalet besökare
		$('.too-many').hide();
		$('.qty input').bind('keyup', function() {
			updatePrice();
		});
		$('input[name="visit-date"]').change(function(){
			updatePrice();
		})
		$('.price.weekend').hide();
		$('.price-descr.weekend').hide();
		//uppdatera prisuppgifter
		function updatePrice() {
			var basePriceTd = $('.price.base-price');
			var basePriceTwoGroupsTd = $('.price.base-price-twogroups');
			var extraCostTd = $('.price.extra-visitors-price');
			var endTimeSpan = $('.end-time');
			var extraVisSpan = $('.price-descr .extra-visitors');
			var baseVisitorsSpan = $('.baseVisitors');
			var maxVisitorsSpan = $('.max-visitors');
			var fikabrodPriceTd = $('.price.fikabrod');
			var weekendPriceTd = $('.price.weekend');
			var vatCostTd = $('.price.vat');
			var totalPriceExTd = $('.price.total-price-ex');
			var totalPriceTd = $('.price.total-price');
			
			var qty = $('.qty input').val();
			if(qty) {
				if( qty != parseInt(qty) || qty != Math.floor(qty) ) {
					var number = ( isNaN(parseInt(qty)) ) ? '' : parseInt(qty);
					$('.qty input').val(number);
				}
			}
			//räkna ut sluttid
			var startTime = $('.visit-time input').val();
			startTime = startTime.replace(':', '.');
			if(startTime) {
				if( startTime != parseFloat(startTime) ) {
					var number = ( isNaN(parseFloat(startTime)) ) ? '' : parseFloat(startTime);
					$('.visit-time input').val(number);
				}
				$('.end-time-label').show();
				if(startTime > 24) {
					console.log('startTime > 24');
					$('.end-time-label').hide();
				}
				var duration = (form == 'techvisit')?	 	3 : 1.3;
				var endTime = parseFloat(startTime) + duration;
				var minutes = Math.round( (endTime - Math.floor(endTime)) * 100);
				if( minutes > 59 ) {
					console.log(endTime);
					endTime = endTime + 1 - 0.60;
				}
				var endTimeString = (Math.round(endTime * 100)/100).toString();

				
				endTimeString = endTimeString.replace('.', ':');
				var parts = endTimeString.split(':');
				if( parseInt(parts[0]) > 23 ) {
					parts[0] = (parseInt(parts[0]) - 24).toString();
				}
				if( endTimeString.indexOf(":") == -1 ) {
					endTimeString = parts[0] + ':00';
				} else {
				  minutes = parts[1];
					if( minutes.length == 0 ){
						endTimeString = parts[0] + ':00';
					} else if( minutes.length == 1 ) {
						endTimeString = parts[0] + ':' + parts[1] + '0';
					} else if( minutes.length > 2) {
						$('.end-time-label').hide();
					}
				}
				endTimeSpan.text(endTimeString);
			} else {
				$('.end-time-label').hide();
			}
			

			var vat = 									0.25;
			var basePrice = 							5600;

			var extraCostPerVis = 						200;
			var basePrice = (form == 'techvisit')?	 	5600 : 3000;
			var fikaCostPerPerson = 					20;

			var extraCost = 							0;
			var totalPriceEx = 							0;
			var totalPrice =							0;
			var fikaCost = 								0;
			var extraVis = 								0;
			var vatCost = 								0;
			var baseVisitors = (form == 'techvisit')?	20 : 25;
			var maxVisitors = (form == 'techvisit')?	30 : 60;
			var weekendPrice =							0;

			var weekDay = dayOfWeek($('input[name="visit-date"]').val());
			if( weekDay == 6 || weekDay == 0 ) {
				weekendPrice = (form == 'techvisit')?	800 : 600;
			}
			
			if ($('input[value="student-discount"]:checked').length) {
				extraCostPerVis = 						100;
				basePrice = (form == 'techvisit')?		3200 : 1600;
			}
			//anpassa för 2 grupper på guidade turer
			if(form != 'techvisit') {
				if(qty > 30) {
					basePrice = 5600;
					baseVisitors = 50;
					if ($('input[value="student-discount"]:checked').length) {
						basePrice = 3000;
					}
				}
			}

			if( Math.floor(qty) == qty && $.isNumeric(qty)) {
				if(!qty) {
					extraVis = 							0;
				}
				extraVis = Math.max( (qty - baseVisitors), 0);
				if ($('input[value="fikabrod"]:checked').length) {
					fikaCost = qty * fikaCostPerPerson;
				}
			}

			

			if(qty > maxVisitors) {
				$('.too-many').show();
			} else {
				$('.too-many').hide();
				//$('.qty input').val(qty);
			}

			if(weekendPrice == 0) {
				weekendPriceTd.hide();
				$('.price-descr.weekend').hide();
			} else {
				weekendPriceTd.show();
				$('.price-descr.weekend').show();
			}
			extraCost = extraVis * extraCostPerVis;
			totalPriceEx = basePrice + extraCost + fikaCost + weekendPrice;
			vatCost = Math.round(totalPriceEx * vat);
			totalPrice = totalPriceEx + vatCost;

			//skriv ut priserna i spans
			basePriceTd.text( priceFormat(basePrice) );
			if(form != 'techvisit') {
				if(qty > 30) {
					basePriceTwoGroupsTd.text( priceFormat(basePrice) );
					basePriceTd.text( priceFormat(0) );
				}
				if(qty <= 30)
					basePriceTwoGroupsTd.text( priceFormat(0) );
			}
			if( baseVisitors == 50 )
				baseVisitors = 25;
			baseVisitorsSpan.text( baseVisitors );
			maxVisitorsSpan.text( maxVisitors );
			extraVisSpan.text(extraVis);
			extraCostTd.text( priceFormat(extraCost) );
			fikabrodPriceTd.text( priceFormat(fikaCost) );
			weekendPriceTd.text( weekendPrice );
			totalPriceExTd.text( priceFormat(totalPriceEx) );
			vatCostTd.text( priceFormat(vatCost) );
			totalPriceTd.text( priceFormat(totalPrice) );

			//uppdatera hidden fields
			$('.hidden.basePrice').val( priceFormat(basePrice) );
			$('.hidden.extraVisitorPrice').val( priceFormat(extraCost) );
			$('.hidden.fikabrodPrice').val( priceFormat(fikaCost) );
			$('.hidden.weekendPrice').val( priceFormat(weekendPrice) );
			$('.hidden.totalExVatPrice').val( priceFormat(totalPriceEx) );
			$('.hidden.vatPrice').val( priceFormat(vatCost) );
			$('.hidden.totalPrice').val( priceFormat(totalPrice) );
		}
		updatePrice();
		//validera
		$('.wpcf7-submit').click(function(e){
			var thisFormsWrapperId = $(this).parents('div.wpcf7').attr('id');
			var reqFields = $( '#' + thisFormsWrapperId + ' *[data-field-required="true"]' );
			console.log(reqFields);
			var send = true;
			reqFields.each(function(){
				$(this).removeClass('empty-error');
				$('.empty-fields-notice').hide();
				//kolla betalningsmetoden om adressen måste fyllas i
				var depName = $(this).attr('data-dependant-on');
				if(depName) {
					var depValue = $(this).attr('data-dependat-value');
					var $depInput = $('input[value="' + depValue + '"]');
					if(!$depInput.is(':checked')) {
						return false;
					}
				}
				
				if($(this).val() == '') {
					send = false;
					$(this).addClass('empty-error');
				}
			});
			if(!send) {
				$('.empty-fields-notice').fadeIn();
				e.preventDefault();
			}
		});
	}

	//takträdgårds-bild
	if($('#post-13 .field_1 img').length > 0 ){
		var $map = $('.field_1 img');

		for( i = 3; i < 40; i=i+2 ) {
		 	if($('.field_' + i).length == 0) break;
			var $textDiv = $('.field_' + i);
			$('.field_1 figure').append('<div class="roof-area roof-area-' + i + '" data-show=".field_' + i + '"></div>');
			$('.roof-area-' + i).click(
			  function(){
			    $('.roof-area').removeClass('active');
			    $(this).addClass('active');
			    var classToShow = $(this).attr('data-show');
			    for( j = 3; j < 40; j=j+2 ) {
  			    $('.field_' + j).hide();
			    }
  			  $(classToShow).show();
			  }
			);
		}
		$('.roof-area').show();
	}
});



















