<?php
/**
 *  Install Add-ons
 *  
 *  The following code will include all 4 premium Add-Ons in your theme.
 *  Please do not attempt to include a file which does not exist. This will produce an error.
 *  
 *  The following code assumes you have a folder 'add-ons' inside your theme.
 *
 *  IMPORTANT
 *  Add-ons may be included in a premium theme/plugin as outlined in the terms and conditions.
 *  For more information, please read:
 *  - http://www.advancedcustomfields.com/terms-conditions/
 *  - http://www.advancedcustomfields.com/resources/getting-started/including-lite-mode-in-a-plugin-theme/
 */ 

// Tillägg 
// include_once( '../wp-content/plugins/acf-repeater/acf-repeater.php');
// include_once('add-ons/acf-gallery/acf-gallery.php');
// include_once('add-ons/acf-flexible-content/acf-flexible-content.php');
// include_once( 'add-ons/acf-options-page/acf-options-page.php' );


/**
 *  Registrera fältgrupper
 *
 *  Funktionen register_field_group tar emot en array som innehåller inställningarna för samtliga fältgrupper.
 *  Du kan redigera denna array fritt. Detta kan dock leda till fel om ändringarna inte är kompatibla med ACF.
 */

function tab( $id, $label ) {
  return array (
  	'key' => $id,
  	'label' => $label,
  	'name' => $label,
  	'type' => 'tab',
  );
}
//––––––––––––––––––––––––––––––––––––––––––––––––––––––––––hem:
$fields = array();
$fields[] = tab('tab_home_1', 'Slider');
$fields[] = array (
	'key' => 'field_5272ab80d8ac1',
	'label' => 'Bild',
	'name' => 'bild',
	'type' => 'repeater',
	'sub_fields' => array (
		array (
			'key' => 'field_5272ab9cd8ac2',
			'label' => 'Bild',
			'name' => 'image',
			'type' => 'image',
			'instructions' => 'Dimensioner 2000 x 444 px',
			'column_width' => 50,
			'save_format' => 'id',
			'preview_size' => 'thumbnail',
			'library' => 'all',
		),
		array (
			'key' => 'field_5272abd0d8ac3',
			'label' => 'Länk till...',
			'name' => 'link_to',
			'type' => 'page_link',
			'column_width' => 50,
			'post_type' => array (
				0 => 'all',
			),
			'taxonomy' => array (
				0 => 'all',
			),
			'allow_null' => 1,
			'multiple' => 0,
		),
		array (
			'key' => 'field_5272ac46d8ac4',
			'label' => 'Text Svenska',
			'name' => 'text',
			'type' => 'wysiwyg',
			'column_width' => 50,
			'default_value' => '',
			'toolbar' => 'full',
			'media_upload' => 'no',
		),
		array (
			'key' => 'field_5272ac86394c5',
			'label' => 'Text engelska',
			'name' => 'text_en',
			'type' => 'wysiwyg',
			'column_width' => 50,
			'default_value' => '',
			'toolbar' => 'full',
			'media_upload' => 'no',
		),
	),
	'row_min' => 0,
	'row_limit' => '',
	'layout' => 'row',
	'button_label' => 'Lägg till bild',
);
$fields[] = tab('tab_home_2', 'Pushar');
for( $i=1; $i<=4; $i++ ) {
  //bild
  $fields[] = array (
		'key' => 'field_icon_' . $i,
		'label' => 'Pushikon ' . $i,
		'name' => 'push_img_' . $i,
		'type' => 'image',
		'save_format' => 'id',
		'preview_size' => 'small',
		'library' => 'all',
	);
  //text
  $fields[] = array (
		'key' => 'field_ptext_' . $i,
		'label' => 'Svensk text till pushikon ' . $i,
		'name' => 'push_text_' . $i,
		'type' => 'wysiwyg',
		'default_value' => '',
		'toolbar' => 'full',
		'media_upload' => 'no',
	);
	$fields[] = array (
		'key' => 'field_ptext_en_' . $i,
		'label' => 'Engelsk text till pushikon ' . $i,
		'name' => 'push_text_' . $i . '_en',
		'type' => 'wysiwyg',
		'default_value' => '',
		'toolbar' => 'full',
		'media_upload' => 'no',
	);
	//länk
  $fields[] = array (
		'key' => 'field_plink_' . $i,
		'label' => 'Länk till pushikon ' . $i,
		'name' => 'push_link_' . $i,
		'type' => 'page_link',
		'post_type' => array (
			0 => 'all',
		),
		'allow_null' => 1,
		'multiple' => 0,
	);
}
$fields[] = tab('tab_home_3', 'Platinamedlemmar');
$fields[] = array (
	'key' => 'field_5272aca9394c61',
	'label' => 'Platinamedlemmar',
	'name' => 'platina_members',
	'type' => 'repeater',
	'sub_fields' => array (
		array (
			'key' => 'field_5272acf5394c71',
			'label' => 'Bild',
			'name' => 'image',
			'type' => 'image',
			'column_width' => 40,
			'save_format' => 'id',
			'preview_size' => 'thumbnail',
			'library' => 'all',
		),
		array (
			'key' => 'field_5272ad9b394c81',
			'label' => 'Länka till...',
			'name' => 'link',
			'type' => 'text',
			'instructions' => 'Ange url för medlemmen',
			'column_width' => 30,
			'default_value' => 'http://',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'formatting' => 'html',
			'maxlength' => '',
		),
		array (
			'key' => 'field_526ba833de8392',
			'label' => 'Namnet på medlemmen',
			'name' => 'hover_text',
			'type' => 'text',
			'instructions' => 'Visas som pektext på sidan',
			'column_width' => 30,
			'default_value' => '',
			'prepend' => '',
			'append' => '',
			'formatting' => 'none',
			'maxlength' => '',
		),
	),
	'row_min' => 0,
	'row_limit' => '',
	'layout' => 'table',
	'button_label' => 'Lägg till medlem',
);
$fields[] = tab('tab_home_4', 'Guldmedlemmar');
$fields[] = array (
	'key' => 'field_5272aca9394c6',
	'label' => 'Guldmedlemmar',
	'name' => 'gold_members',
	'type' => 'repeater',
	'sub_fields' => array (
		array (
			'key' => 'field_5272acf5394c7',
			'label' => 'Bild',
			'name' => 'image',
			'type' => 'image',
			'column_width' => 40,
			'save_format' => 'id',
			'preview_size' => 'thumbnail',
			'library' => 'all',
		),
		array (
			'key' => 'field_5272ad9b394c8',
			'label' => 'Länka till...',
			'name' => 'link',
			'type' => 'text',
			'instructions' => 'Ange url för medlemmen',
			'column_width' => 30,
			'default_value' => 'http://',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'formatting' => 'html',
			'maxlength' => '',
		),
		array (
			'key' => 'field_526ba833de8391',
			'label' => 'Namnet på medlemmen',
			'name' => 'hover_text',
			'type' => 'text',
			'instructions' => 'Visas som pektext på sidan',
			'column_width' => 30,
			'default_value' => '',
			'prepend' => '',
			'append' => '',
			'formatting' => 'none',
			'maxlength' => '',
		),
	),
	'row_min' => 0,
	'row_limit' => '',
	'layout' => 'table',
	'button_label' => 'Lägg till medlem',
);
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_hem',
		'title' => 'Hem',
		'fields' => $fields,
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-hem.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'the_content',
				1 => 'excerpt',
				2 => 'custom_fields',
				3 => 'discussion',
				4 => 'comments',
				5 => 'revisions',
				7 => 'author',
				8 => 'format',
				9 => 'featured_image',
				10 => 'categories',
				11 => 'tags',
				12 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
}

//–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––events
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_event-info',
		'title' => 'Event-info',
		'fields' => array (
			array (
				'key' => 'field_526ba833de839',
				'label' => 'Arrangör (på svenska)',
				'name' => 'arranger',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => 'Arrangör',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_526ba833d1234',
				'label' => 'Arranged by (in english)',
				'name' => 'arranger_en',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => 'Arranged by',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_526ba833de123',
				'label' => 'Plats',
				'name' => 'location',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => 'Plats',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'price_field',
				'label' => 'Pris',
				'name' => 'price',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => 'Pris',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'url_field',
				'label' => 'Länk',
				'name' => 'url',
				'type' => 'text',
				'instructions' => 'Lämna tomt om det inte finns någon.',
				'default_value' => '',
				'placeholder' => 'http://',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_526ba4e6e0897',
				'label' => 'Startdatum',
				'name' => 'startdate',
				'type' => 'date_picker',
				'instructions' => 'Om detta inte fylls i kommer eventet inte synas i kalendern.',
				'date_format' => 'yy-mm-dd',
				'display_format' => 'yy-mm-dd',
				'first_day' => 1,
			),
			array (
				'key' => 'field_526ba79fc0cf1',
				'label' => 'Slutdatum',
				'name' => 'enddate',
				'type' => 'date_picker',
				'instructions' => 'Fyll i om eventet sträcker sig över flera dagar, annars går det bra att lämna den tom.',
				'date_format' => 'yy-mm-dd',
				'display_format' => 'yy-mm-dd',
				'first_day' => 1,
			),
			array (
				'key' => 'field_526ba62cf2231',
				'label' => 'Starttid',
				'name' => 'starttime',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '10:00',
				'prepend' => '(HH:MM)',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => 5,
			),
			array (
				'key' => 'field_526ba801c0cf2',
				'label' => 'Sluttid',
				'name' => 'endtime',
				'type' => 'text',
				'instructions' => 'Kan lämnas tom',
				'default_value' => '',
				'placeholder' => '12:00',
				'prepend' => '(HH:MM)',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => 5,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'event',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}


//–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––twocol
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_twocol',
		'title' => 'Två kolumner',
		'fields' => array (
			array (
				'key' => 'field_527243752356',
				'label' => 'Fördelning på kolumnerna',
				'name' => 'col_1_width',
				'type' => 'radio',
				'choices' => array (
					'3' => '25% - 75%',
					'4' => '33% - 66%',
					'6' => '50% - 50%',
					'8' => '66% - 33%',
					'9' => '75% - 25%',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => '6',
				'layout' => 'horizontal',
			),
			tab( 'field_tab_twocol_se', 'Svenska' ),
			array (
				'key' => 'field_twocol_full',
				'label' => 'Full bredd',
				'name' => 'col_full',
				'type' => 'wysiwyg',
				'instructions' => 'Använd som introduktion som presenteras i fullbredd. Eller lämna tom för att köra på kolumnlayouten direkt!',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
			array (
				'key' => 'field_col1',
				'label' => 'Vänster',
				'name' => 'col_1',
				'type' => 'wysiwyg',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
			array (
				'key' => 'field_col2',
				'label' => 'Höger',
				'name' => 'col_2',
				'type' => 'wysiwyg',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
			tab( 'field_tab_twocol_en', 'English' ),
			array (
				'key' => 'field_twocol_full_en',
				'label' => 'Full bredd',
				'name' => 'col_full_en',
				'type' => 'wysiwyg',
				'instructions' => 'Använd som introduktion som presenteras i fullbredd. Eller lämna tom för att köra på kolumnlayouten direkt!',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
			array (
				'key' => 'field_col1_en',
				'label' => 'Left',
				'name' => 'col_1_en',
				'type' => 'wysiwyg',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
			array (
				'key' => 'field_col2_en',
				'label' => 'Right',
				'name' => 'col_2_en',
				'type' => 'wysiwyg',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-guided-tours.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-twocol.php',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'the_content',
				1 => 'excerpt',
				2 => 'custom_fields',
				3 => 'discussion',
				4 => 'comments',
				7 => 'author',
				8 => 'format',
				9 => 'featured_image',
				10 => 'categories',
				11 => 'tags',
				12 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
}

//–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––threecol
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_threecol',
		'title' => 'Fyra kolumner',
		'fields' => array (
			tab( 'field_tab_threecol_se', 'Svenska' ),
			array (
				'key' => 'field_threecol_full',
				'label' => 'Full bredd',
				'name' => 'col_full',
				'type' => 'wysiwyg',
				'instructions' => 'Använd som introduktion som presenteras i fullbredd. Eller lämna tom för att köra på kolumnlayouten direkt!',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
			array (
				'key' => 'field_col1',
				'label' => 'Kolumn 1',
				'name' => 'col_1',
				'type' => 'wysiwyg',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
			array (
				'key' => 'field_col2',
				'label' => 'Kolumn 2',
				'name' => 'col_2',
				'type' => 'wysiwyg',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
			array (
				'key' => 'field_col3',
				'label' => 'Kolumn 3',
				'name' => 'col_3',
				'type' => 'wysiwyg',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
			tab( 'field_tab_threecol_en', 'English' ),
			array (
				'key' => 'field_threecol_full_en',
				'label' => 'Full bredd',
				'name' => 'col_full_en',
				'type' => 'wysiwyg',
				'instructions' => 'Använd som introduktion som presenteras i fullbredd. Eller lämna tom för att köra på kolumnlayouten direkt!',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
			array (
				'key' => 'field_col1_en',
				'label' => 'Column 1',
				'name' => 'col_1_en',
				'type' => 'wysiwyg',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
			array (
				'key' => 'field_col2_en',
				'label' => 'Column 2',
				'name' => 'col_2_en',
				'type' => 'wysiwyg',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
			array (
				'key' => 'field_col3_en',
				'label' => 'Column 3',
				'name' => 'col_3_en',
				'type' => 'wysiwyg',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-threecol.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'the_content',
				1 => 'excerpt',
				2 => 'custom_fields',
				3 => 'discussion',
				4 => 'comments',
				7 => 'author',
				8 => 'format',
				9 => 'featured_image',
				10 => 'categories',
				11 => 'tags',
				12 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
}

//–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––fourcol
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_fourcol',
		'title' => 'Fyra kolumner',
		'fields' => array (
			tab( 'field_tab_fourcol_se', 'Svenska' ),
			array (
				'key' => 'field_fourcol_full',
				'label' => 'Full bredd',
				'name' => 'col_full',
				'type' => 'wysiwyg',
				'instructions' => 'Använd som introduktion som presenteras i fullbredd. Eller lämna tom för att köra på kolumnlayouten direkt!',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
			array (
				'key' => 'field_col1',
				'label' => 'Kolumn 1',
				'name' => 'col_1',
				'type' => 'wysiwyg',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
			array (
				'key' => 'field_col2',
				'label' => 'Kolumn 2',
				'name' => 'col_2',
				'type' => 'wysiwyg',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
			array (
				'key' => 'field_col3',
				'label' => 'Kolumn 3',
				'name' => 'col_3',
				'type' => 'wysiwyg',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
			array (
				'key' => 'field_col4',
				'label' => 'Kolumn 4',
				'name' => 'col_4',
				'type' => 'wysiwyg',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
			tab( 'field_tab_fourcol_en', 'English' ),
			array (
				'key' => 'field_fourcol_full_en',
				'label' => 'Full bredd',
				'name' => 'col_full_en',
				'type' => 'wysiwyg',
				'instructions' => 'Använd som introduktion som presenteras i fullbredd. Eller lämna tom för att köra på kolumnlayouten direkt!',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
			array (
				'key' => 'field_col1_en',
				'label' => 'Column 1',
				'name' => 'col_1_en',
				'type' => 'wysiwyg',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
			array (
				'key' => 'field_col2_en',
				'label' => 'Column 2',
				'name' => 'col_2_en',
				'type' => 'wysiwyg',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
			array (
				'key' => 'field_col3_en',
				'label' => 'Column 3',
				'name' => 'col_3_en',
				'type' => 'wysiwyg',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
			array (
				'key' => 'field_col4_en',
				'label' => 'Column 4',
				'name' => 'col_4_en',
				'type' => 'wysiwyg',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-fourcol.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'the_content',
				1 => 'excerpt',
				2 => 'custom_fields',
				3 => 'discussion',
				4 => 'comments',
				7 => 'author',
				8 => 'format',
				9 => 'featured_image',
				10 => 'categories',
				11 => 'tags',
				12 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
}

//–––––––––––––––––––––––––––––––––––––––––––––––––––––––––– technical visits:
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_visit',
		'title' => 'Kursmoment',
		'fields' => array (
			tab( 'field_visit_tab_1', 'Svenska' ),
			array (
				'key' => 'field_5270f487bfa80',
				'label' => 'Moment på svenska',
				'name' => 'moment',
				'type' => 'repeater',
				'instructions' => 'Lägg till, ta bort och flytta runt bland momenten',
				'sub_fields' => array (
					array (
						'key' => 'field_52724375cda86',
						'label' => 'Typ',
						'name' => 'moment_type',
						'type' => 'radio',
						'column_width' => 25,
						'choices' => array (
							'Föreläsning' => 'Föreläsning',
							'Tur' => 'Tur',
							'Rast' => 'Rast',
							'Workshop' => 'Workshop',
						),
						'other_choice' => 0,
						'save_other_choice' => 0,
						'default_value' => '',
						'layout' => 'vertical',
					),
					array (
						'key' => 'field_52724458cda87',
						'label' => 'Beskrivning',
						'name' => 'description',
						'type' => 'text',
						'column_width' => 75,
						'default_value' => '',
						'toolbar' => 'full',
						'media_upload' => 'no',
						'type' => 'text',
						'formatting' => 'none',
						'maxlength' => 1000,
					),
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'table',
				'button_label' => 'Lägg till nytt moment',
			),
			tab( 'field_visit_tab_2', 'English' ),
			array (
				'key' => 'field_5270f487bfa81',
				'label' => 'Moments in english',
				'name' => 'moment_en',
				'type' => 'repeater',
				'instructions' => 'Lägg till, ta bort och flytta runt bland momenten',
				'sub_fields' => array (
					array (
						'key' => 'field_52724375cda88',
						'label' => 'Typ',
						'name' => 'moment_type_en',
						'type' => 'radio',
						'column_width' => 25,
						'choices' => array (
							'Talk' => 'Talk',
							'Walk' => 'Walk',
							'Break' => 'Break',
							'Workshop' => 'Workshop',
						),
						'other_choice' => 0,
						'save_other_choice' => 0,
						'default_value' => '',
						'layout' => 'vertical',
					),
					array (
						'key' => 'field_52724458cda89',
						'label' => 'Description',
						'name' => 'description_en',
						'type' => 'text',
						'column_width' => 75,
						'default_value' => '',
						'formatting' => 'none',
						'maxlength' => 1000,
					),
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'table',
				'button_label' => 'Add new moment',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'visit',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				2 => 'custom_fields',
				3 => 'discussion',
				4 => 'comments',
				7 => 'author',
				8 => 'format',
				9 => 'featured_image',
				10 => 'categories',
				11 => 'tags',
				12 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
}

//–––––––––––––––––––––––––––––––––––––––––––––––––––––––––– tabbar:
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_tabs',
		'title' => 'Vertikala flikar',
		'fields' => array (
			array (
				'key' => 'field_tab_full',
				'label' => 'Full bredd',
				'name' => 'tab_full',
				'type' => 'wysiwyg',
				'instructions' => 'Använd som introduktion som presenteras i fullbredd',
	    		'default_value' => '',
	    		'toolbar' => 'full',
	    		'media_upload' => 'yes',
			),
			tab( 'field_tabs_tab_1', 'Svenska' ),
			array (
				'key' => 'field_5270f487bfa82',
				'label' => 'Flikar på svenska',
				'name' => 'tabs',
				'type' => 'repeater',
				'instructions' => 'Lägg till, ta bort och flytta runt bland flikarna',
				'sub_fields' => array (
					array (
						'key' => 'field_52724458cda90',
						'label' => 'Rubrik',
						'name' => 'heading',
						'type' => 'text',
						'column_width' => 25,
						'default_value' => '',
						'toolbar' => 'full',
						'formatting' => 'none',
						'maxlength' => 1000,
					),
					array (
						'key' => 'field_52724458cda91',
						'label' => 'Innehåll',
						'name' => 'content',
						'type' => 'wysiwyg',
						'instructions' => 'Använd som introduktion som presenteras i fullbredd',
						'column_width' => 75,
			    		'default_value' => '',
			    		'toolbar' => 'full',
			    		'media_upload' => 'yes',
					),
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'table',
				'button_label' => 'Lägg till ny flik',
			),
			tab( 'field_tabs_tab_2', 'English' ),
			array (
				'key' => 'field_5270f487bfa82_en',
				'label' => 'Tabs in english',
				'name' => 'tabs_en',
				'type' => 'repeater',
				'instructions' => 'Lägg till, ta bort och flytta runt bland flikarna',
				'sub_fields' => array (
					array (
						'key' => 'field_52724458cda90_en',
						'label' => 'Heading',
						'name' => 'heading_en',
						'type' => 'text',
						'column_width' => 25,
						'default_value' => '',
						'toolbar' => 'full',
						'formatting' => 'none',
						'maxlength' => 1000,
					),
					array (
						'key' => 'field_52724458cda91_en',
						'label' => 'Content',
						'name' => 'content_en',
						'type' => 'wysiwyg',
						'instructions' => 'Använd som introduktion som presenteras i fullbredd',
						'column_width' => 75,
			    		'default_value' => '',
			    		'toolbar' => 'full',
			    		'media_upload' => 'yes',
					),
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'table',
				'button_label' => 'Lägg till ny flik',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-tabs.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				1 => 'the_content',
				2 => 'custom_fields',
				3 => 'discussion',
				4 => 'comments',
				7 => 'author',
				8 => 'format',
				9 => 'featured_image',
				10 => 'categories',
				11 => 'tags',
				12 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
}


//–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––multisida
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_sidor-att-lagga-in',
		'title' => 'Sidor att lägga in',
		'fields' => array (
			array (
				'key' => 'field_527a4ca0e53c6',
				'label' => __('Sidor'),
				'name' => 'pages',
				'type' => 'relationship',
				'return_format' => 'id',
				'post_type' => array (
					0 => 'page',
				),
				'taxonomy' => array (
					0 => 'all',
				),
				'filters' => array (
					0 => 'search',
					1 => 'post_type',
				),
				'result_elements' => array (
					0 => 'featured_image',
					1 => 'post_type',
					2 => 'post_title',
				),
				'max' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-multi.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'excerpt',
				1 => 'custom_fields',
				2 => 'discussion',
				3 => 'comments',
				4 => 'author',
				5 => 'format',
				6 => 'featured_image',
			),
		),
		'menu_order' => 0,
	));
}
//–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––orbit
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_slider',
		'title' => 'Bilder',
		'fields' => array (
			array (
				'key' => 'field_54ab80d8ac1',
				'label' => 'Bild',
				'name' => 'orbit_bild',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_5272539cd8ac2',
						'label' => 'Bild',
						'name' => 'image',
						'type' => 'image',
						'instructions' => 'Dimensioner ? x ? px',
						'column_width' => 50,
						'save_format' => 'id',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_52723d0d8ac3',
						'label' => 'Länk till...',
						'name' => 'link_to',
						'type' => 'page_link',
						'column_width' => 50,
						'post_type' => array (
							0 => 'all',
						),
						'taxonomy' => array (
							0 => 'all',
						),
						'allow_null' => 1,
						'multiple' => 0,
					),
					array (
						'key' => 'field_5272a46d8ac4',
						'label' => 'Text Svenska',
						'name' => 'text',
						'type' => 'wysiwyg',
						'column_width' => 50,
						'default_value' => '',
						'toolbar' => 'full',
						'media_upload' => 'no',
					),
					array (
						'key' => 'field_5242a4394c5',
						'label' => 'Text engelska',
						'name' => 'text_en',
						'type' => 'wysiwyg',
						'column_width' => 50,
						'default_value' => '',
						'toolbar' => 'full',
						'media_upload' => 'no',
					),
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'row',
				'button_label' => 'Lägg till bild',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-orbit.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'the_content',
				1 => 'excerpt',
				2 => 'custom_fields',
				3 => 'discussion',
				4 => 'comments',
				7 => 'author',
				8 => 'format',
				9 => 'featured_image',
				10 => 'categories',
				11 => 'tags',
				12 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
}
//--------------------------------------------------standard page:
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_standardmall',
		'title' => 'Standardmall',
		'fields' => array (
			array (
				'key' => 'field_52961719aa8d2',
				'label' => __('Rad'),
				'name' => 'rad',
				'type' => 'repeater',
				'instructions' => __('Flera rader kan skapas, varje rad kan få högst en bild'),
				'sub_fields' => array (
					array (
						'key' => 'field_5296172caa8d3',
						'label' => __('Layout'),
						'name' => 'layout',
						'type' => 'radio',
						'choices' => array (
							4 => 'Bild till vänster',
							8 => 'Bild till höger',
						),
						'other_choice' => 0,
						'save_other_choice' => 0,
						'default_value' => 8,
						'layout' => 'vertical',
					),
					array (
						'key' => 'field_5272539cd8ac2',
						'label' => 'Bild',
						'name' => 'image',
						'type' => 'image',
						'save_format' => 'id',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_5296179aaa8d4',
						'label' => __('Text Svenska'),
						'name' => 'text',
						'type' => 'wysiwyg',
						'column_width' => '',
						'default_value' => '',
						'toolbar' => 'full',
						'media_upload' => 'yes',
					),
					array (
						'key' => 'field_529617c5aa8d5',
						'label' => __('Text English'),
						'name' => 'text_en',
						'type' => 'wysiwyg',
						'column_width' => '',
						'default_value' => '',
						'toolbar' => 'full',
						'media_upload' => 'yes',
					),
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'row',
				'button_label' => 'Lägg till rad',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
					'order_no' => 0,
					'group_no' => 0,
				),
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'default',
					'order_no' => 1,
					'group_no' => 0,
				),
				array (
					'param' => 'page',
					'operator' => '!=',
					'value' => '702',
					'order_no' => 1,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'the_content',
			),
		),
		'menu_order' => 0,
	));
}

