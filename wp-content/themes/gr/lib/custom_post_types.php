<?php

// Register Custom Post Type
function custom_post_type() {
  
  //events:
	$labels = array(
		'name'                => _x( 'Events', 'Post Type General Name', 'reverie' ),
		'singular_name'       => _x( 'Event', 'Post Type Singular Name', 'reverie' ),
		'menu_name'           => __( 'Events', 'reverie' ),
		'parent_item_colon'   => __( 'Parent event:', 'reverie' ),
		'all_items'           => __( 'Alla events', 'reverie' ),
		'view_item'           => __( 'Se event', 'reverie' ),
		'add_new_item'        => __( 'Lägg till nytt event', 'reverie' ),
		'add_new'             => __( 'Nytt event', 'reverie' ),
		'edit_item'           => __( 'Redigera event', 'reverie' ),
		'update_item'         => __( 'Uppdatera event', 'reverie' ),
		'search_items'        => __( 'Sök bland events', 'reverie' ),
		'not_found'           => __( 'Inga events funna', 'reverie' ),
		'not_found_in_trash'  => __( 'Inga events i papperskorgen', 'reverie' ),
	);
	$rewrite = array(
		'slug'                => 'events',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'label'               => __( 'events', 'reverie' ),
		'description'         => __( 'Kalenderhändelser', 'reverie' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		//'menu_icon'           => '',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'page',
	);
	register_post_type( 'event', $args );
	
	//Tekniska besök:
	$labels = array(
		'name'                => _x( 'Tekniskt Besök', 'Post Type General Name', 'reverie' ),
		'singular_name'       => _x( 'Tekniskt Besök', 'Post Type Singular Name', 'reverie' ),
		'menu_name'           => __( 'Tekniska Besök', 'reverie' ),
		'parent_item_colon'   => __( 'Parent Tekniska Besök:', 'reverie' ),
		'all_items'           => __( 'Alla Tekniska Besök', 'reverie' ),
		'view_item'           => __( 'Se Tekniskt Besök', 'reverie' ),
		'add_new_item'        => __( 'Lägg till Tekniskt Besök', 'reverie' ),
		'add_new'             => __( 'Nytt Tekniskt Besök', 'reverie' ),
		'edit_item'           => __( 'Redigera Tekniskt Besök', 'reverie' ),
		'update_item'         => __( 'Uppdatera Tekniskt Besök', 'reverie' ),
		'search_items'        => __( 'Sök bland Tekniska Besök', 'reverie' ),
		'not_found'           => __( 'Inga Tekniska Besök funna', 'reverie' ),
		'not_found_in_trash'  => __( 'Inga Tekniska Besök i papperskorgen', 'reverie' ),
	);
	$rewrite = array(
		'slug'                => 'technical-visits',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'label'               => __( 'Tekniska Besök', 'reverie' ),
		'description'         => __( 'Tekniska Besök', 'reverie' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt'),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		//'menu_icon'           => '',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'page',
	);
	register_post_type( 'visit', $args );
}

// Hook into the 'init' action
add_action( 'init', 'custom_post_type', 0 );