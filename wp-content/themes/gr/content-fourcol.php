<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
	<header>
		<h1 class="entry-title"><?php the_title(); ?></h1>
	</header>

	<?php
		$content = get_field_translated( 'field_fourcol_full' );
		//echo implode("-", str_split($content, 1));
		if( !empty( $content ) ) { ?>
		<div class="entry-content row full-width">
				<section class="large-12 columns full-column">
				<?php echo $content; ?>
				</section>
		</div>
		<?php }
	?>

	<div class="entry-content row four-columns">
		<section class="small-6 large-3 columns left-column">
			<?php the_field_translated( 'col_1' ); ?>
		</section>
		<section class="small-6 large-3 columns">
			<?php the_field_translated( 'col_2' ); ?>
		</section>
		<section class="small-6 large-3 columns">
			<?php the_field_translated( 'col_3' ); ?>
		</section>
		<section class="small-6 large-3 columns right-column">
			<?php the_field_translated( 'col_4' ); ?>
		</section>
	</div>
</article>