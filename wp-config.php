<?php
/**
 * Baskonfiguration f�r WordPress.
 *
 * Denna fil inneh�ller f�ljande konfigurationer: Inst�llningar f�r MySQL,
 * Tabellprefix, S�kerhetsnycklar, WordPress-spr�k, och ABSPATH.
 * Mer information p� {@link http://codex.wordpress.org/Editing_wp-config.php 
 * Editing wp-config.php}. MySQL-uppgifter f�r du fr�n ditt webbhotell.
 *
 * Denna fil anv�nds av wp-config.php-genereringsskript under installationen.
 * Du beh�ver inte anv�nda webbplatsen, du kan kopiera denna fil direkt till
 * "wp-config.php" och fylla i v�rdena.
 *
 * @package WordPress
 */

// ** MySQL-inst�llningar - MySQL-uppgifter f�r du fr�n ditt webbhotell ** //
/** Namnet p� databasen du vill anv�nda f�r WordPress */
define('DB_NAME', 'greenroof');

/** MySQL-databasens anv�ndarnamn */
define('DB_USER', 'root');

/** MySQL-databasens l�senord */
define('DB_PASSWORD', 'ett2tre4fem');

/** MySQL-server */
define('DB_HOST', 'localhost');

/** Teckenkodning f�r tabellerna i databasen. */
define('DB_CHARSET', 'utf8');

/** Kollationeringstyp f�r databasen. �ndra inte om du �r os�ker. */
define('DB_COLLATE', '');

/**#@+
 * Unika autentiseringsnycklar och salter.
 *
 * �ndra dessa till unika fraser!
 * Du kan generera nycklar med {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Du kan n�r som helst �ndra dessa nycklar f�r att g�ra aktiva cookies obrukbara, vilket tvingar alla anv�ndare att logga in p� nytt.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'oh)r`aZ!APYp&zwA~TmAI8Wbo/}o2&kixE_GfvlXsAXB2drQtDd/fFty|6#3|JMT');
define('SECURE_AUTH_KEY',  '?)y;zNyj~Z__H$6C|0Z%k,]SH:hapQizF.l$b)c&`eb|uVhqyH!YAX|Lk]F$gmnY');
define('LOGGED_IN_KEY',    'iR6XT4(6PH,pmb`_}A2?A>0YE)gIm$YtQ+S.Y&j.|w/$b[[vwC]ufSb/<p|wW:,<');
define('NONCE_KEY',        '+N_8:SPxZHx+ByS$4l$*8^VsM{t[U03?z~= e;*a?2UG+Yj2vdX~h;fzX[r}N` W');
define('AUTH_SALT',        '~KQ-/4r=>MsK{lxm aO$DC>=1v{2E+0HWRUUsMtCh^y|QKV :-T.!u(/[xeoyH7H');
define('SECURE_AUTH_SALT', '%qA)?$8!^2+8T&i%!+_&PfN-&KSvSeIdR)NdHl?}.aZTch5j}7c+-WB,Pq0b8R)%');
define('LOGGED_IN_SALT',   'amez?%GTR.,A9(h`)Z++5H(d2,$b*<KjH8{O/X/)P9!j66K|3Pc_at4SfSbdk;Z5');
define('NONCE_SALT',       'Q09}$+v~Loz}1*t|O]%*4FjLp^{Y9vSZR|B!RSJ)(Qsd@)t?g+/~<C]<*IA|Fp5Z');

/**#@-*/

/**
 * Tabellprefix f�r WordPress Databasen.
 *
 * Du kan ha flera installationer i samma databas om du ger varje installation ett unikt
 * prefix. Endast siffror, bokst�ver och understreck!
 */
$table_prefix  = 'gr_';

/**
 * WordPress-spr�k, f�rinst�llt f�r svenska.
 *
 * Du kan �ndra detta f�r att �ndra spr�k f�r WordPress.  En motsvarande .mo-fil
 * f�r det valda spr�ket m�ste finnas i wp-content/languages. Exempel, l�gg till
 * sv_SE.mo i wp-content/languages och ange WPLANG till 'sv_SE' f�r att f� sidan
 * p� svenska.
 */
define('WPLANG', 'sv_SE');

/** 
 * F�r utvecklare: WordPress fels�kningsl�ge. 
 * 
 * �ndra detta till true f�r att aktivera meddelanden under utveckling. 
 * Det �r rekommderat att man som till�ggsskapare och temaskapare anv�nder WP_DEBUG 
 * i sin utvecklingsmilj�. 
 */ 
define('WP_DEBUG', false);

/* Det var allt, sluta redigera h�r! Blogga p�. */

/** Absoluta s�kv�g till WordPress-katalogen. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Anger WordPress-v�rden och inkluderade filer. */
require_once(ABSPATH . 'wp-settings.php');